import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:fluttertoast/fluttertoast.dart';

void main() => runApp(MyApp());
var pin_code = '';
var login_pending = false;

class Post {
  final int userId;
  final int id;
  final String title;
  final String body;

  Post({this.userId, this.id, this.title, this.body});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      body: json['body'],
    );
  }
}

Future<Post> loginOrCreateAccount(pin, context) async {
  login_pending = true;
  final response =
      await http.post('http://51.255.75.225:4000', body: {"pin": pin});

  login_pending = false;
  if (response.statusCode == 200) {
    pin_code = pin;
    Navigator.pushReplacementNamed(context, "/home");
    print(response.body);
    // If server returns an OK response, parse the JSON
    // return Post.fromJson(json.decode(response.body));
  } else {
    Fluttertoast.showToast(
        msg: "Error connection",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        bgcolor: "#e74c3c",
        textcolor: '#ffffff');
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stellar Demo',
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: MyHomePage(title: 'Stellar Demo'),
      routes: {
        "/home": (_) => new SecondScreen(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _loading = false;
  String _code;
  void _goToHome() {
    if (login_pending) {
      return;
    }
    if (this._code.length != 4) {
      Fluttertoast.showToast(
          msg: "Entrez un code à 4 chiffre svp !",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          bgcolor: "#e74c3c",
          textcolor: '#ffffff');
      resetCode();
      return;
    }
    if (this._code.contains('.') ||
        this._code.contains('-') ||
        this._code == "0000") {
      Fluttertoast.showToast(
          msg: "Code invalide !",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          bgcolor: "#e74c3c",
          textcolor: '#ffffff');
      resetCode();
      this._code = '';
      return;
    }
    setState(() {
      _loading = true;
    });
    loginOrCreateAccount(this._code, context);
    resetCode();
  }

  resetCode() {
    setState(() {
      this._code = '';
    });
  }

  @override
  Widget build(BuildContext context) {
    var body_login = new Container(
      padding: const EdgeInsets.all(40.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("Entrer votre code pin"),
          TextField(
            keyboardType: TextInputType.numberWithOptions(),
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Pin code",
            ),
            onChanged: (String code) {
              this._code = code;
            },
          )
        ],
      ),
    );
    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          body_login,
          new Container(
            alignment: AlignmentDirectional.center,
            decoration: new BoxDecoration(
              color: Colors.white70,
            ),
            child: new Container(
              decoration: new BoxDecoration(
                  color: Colors.teal[200],
                  borderRadius: new BorderRadius.circular(10.0)),
              width: 300.0,
              height: 200.0,
              alignment: AlignmentDirectional.center,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Center(
                    child: new SizedBox(
                      height: 50.0,
                      width: 50.0,
                      child: new CircularProgressIndicator(
                        value: null,
                        strokeWidth: 7.0,
                      ),
                    ),
                  ),
                  new Container(
                    margin: const EdgeInsets.only(top: 25.0),
                    child: new Center(
                      child: new Text(
                        "Chargement en cours.. please wait...",
                        style: new TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: new Container(child: _loading ? bodyProgress : body_login),
      floatingActionButton: FloatingActionButton(
        onPressed: _goToHome,
        tooltip: 'Increment',
        child: Icon(Icons.arrow_forward),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class SecondScreen extends StatefulWidget {

  SecondScreen({Key key, this.title}) : super(key: key);

  final String title;
  @override
  State<StatefulWidget> createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  String _montant;
  String _destination;
  bool _loading = false;
  makeTransaction() {
    if (this._destination == null || this._destination.length != 4 || this._destination.contains('.') || this._destination.contains('-') || this._destination == "0000") {
      Fluttertoast.showToast(
          msg: "Code destination invalide !",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          bgcolor: "#e74c3c",
          textcolor: '#ffffff');
      return;
    }
    if (this._montant == null || this._montant == '') {
      Fluttertoast.showToast(
          msg: "Montant invalide !",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          bgcolor: "#e74c3c",
          textcolor: '#ffffff');
      return;
    }
    setState(() {
      _loading = true;
    });
    final response = http.post('http://51.255.75.225:4000/payment', body: {"pin_a": pin_code, "pin_b": this._destination, "amount": this._montant});
    response.then((onValue) => this.res(onValue));
  }
  res(value) {
    setState(() {
      _loading = false;
    });
    if (value.statusCode == 200) {
      Fluttertoast.showToast(
        msg: value.body,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        bgcolor: "#7DE6C2",
        textcolor: '#ffffff');
    } else {
      Fluttertoast.showToast(
        msg: "Ce compte n'existe pas!",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        bgcolor: "#e74c3c",
        textcolor: '#ffffff');
    }
  }

  @override
  Widget build(BuildContext context) {
    var containerTransaction = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        TextField(
          keyboardType: TextInputType.numberWithOptions(),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Montant transaction",
          ),
          onChanged: (String montant) {
            this._montant = montant;
          },
        ),
        TextField(
          keyboardType: TextInputType.numberWithOptions(),
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Destinataire",
          ),
          onChanged: (String dest) {
            this._destination = dest;
          },
        ),
        RaisedButton(
          onPressed: () {
            this.makeTransaction();
          },
          child: Text('Effectuez la transaction'),
        ),
        RaisedButton(
          onPressed: () {
            Navigator.push(context,
                new MaterialPageRoute(builder: (context) => MesTransaction()));
          },
          child: Text('Voir mes transaction'),
        )
      ],
    );

    var bodyProgress = new Container(
      child: new Stack(
        children: <Widget>[
          containerTransaction,
          new Container(
            alignment: AlignmentDirectional.center,
            decoration: new BoxDecoration(
              color: Colors.white70,
            ),
            child: new Container(
              decoration: new BoxDecoration(
                  color: Colors.teal[200],
                  borderRadius: new BorderRadius.circular(10.0)),
              width: 300.0,
              height: 200.0,
              alignment: AlignmentDirectional.center,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Center(
                    child: new SizedBox(
                      height: 50.0,
                      width: 50.0,
                      child: new CircularProgressIndicator(
                        value: null,
                        strokeWidth: 7.0,
                      ),
                    ),
                  ),
                  new Container(
                    margin: const EdgeInsets.only(top: 25.0),
                    child: new Center(
                      child: new Text(
                        "Chargement en cours.. please wait...",
                        style: new TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );

    return Scaffold(
        appBar: AppBar(
          title: Text("Accueil"),
        ),
        body: Container(
            padding: const EdgeInsets.all(40.0), 
            child: _loading? bodyProgress : containerTransaction
          )
        );
  }
}

class MesTransaction extends StatefulWidget {
  MesTransaction({Key key, this.title}) : super(key: key);

  final String title;
  @override
  State<StatefulWidget> createState() => _MesTransactionState();
}

class _MesTransactionState extends State<MesTransaction> {
  String _account_id = '';
  String _account_sequence = '';
  String balance = '';
  bool _loading = false;

  loadAccount() {
    final response = http.post('http://51.255.75.225:4000/account', body: {"pin": pin_code});
    response.then((onValue) => this.res(onValue));
  }

  res(value) {
    setState(() {
      var account = json.decode(value.body);
      this._account_id = account["id"];
      this._account_sequence = account["sequence"]; 
      this.balance = account["balances"][0]["balance"];
    });
  }
  @override
  Widget build(BuildContext context) {
    this.loadAccount();
    var body_info = Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text("Id compte", style: TextStyle(fontWeight: FontWeight.bold)),
        Text(this._account_id),
        Text("sequence", style: TextStyle(fontWeight: FontWeight.bold)),
        Text(this._account_sequence),
        Text("Balance", style: TextStyle(fontWeight: FontWeight.bold)),
        Text(this.balance),
        RaisedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Retour!'),
        ),
      ]
    );
    return Scaffold(
      appBar: AppBar(
        title: Text("Informations compte"),
      ),
      body: Container(
        padding: const EdgeInsets.all(40.0),
        child: body_info
      ),
    );
  }
}
